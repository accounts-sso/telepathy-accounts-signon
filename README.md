# Telepathy integration for the Accounts SSO framework

A mission control plugin for Telepathy, integrating with libaccounts and libsignon to provide IM accounts and authentication.

This code is based on Nemo Mobile's fork of the plugin from Empathy's ubuntu-online-account support.

This code has been updated to not use deprecated methods and adds latest changes from Empathy's source tree.
